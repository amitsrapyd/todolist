import { expect } from "chai";
import { create, read, update, del } from "../src/controller/todo.controller";
import { ITask } from "../src/interfaces/todo.interfaces";

describe("todo-controller", () => {
  context("#create", () => {
    it("should exist", () => {
      expect(create).to.be.a("function");
      expect(create).to.be.instanceOf(Function);
    });

    it("should create and add a new task", () => {
      const mockTodos: ITask[] = [
        { id: "ox3r2kn2fzr", isDone: false, text: "first todo" },
        { id: "4csvd9s5w7u", isDone: true, text: "second todo" }
      ];

      const newTodos = create(mockTodos, "new created todo");
      const newTodo: ITask = { ...newTodos[newTodos.length - 1] };

      expect(newTodo.isDone).to.be.equal(false);
      expect(newTodo.text).to.be.equal("new created todo");
      expect(newTodo.id).to.be.a("string").and.has.length.greaterThanOrEqual(1);
    });
  });

  context("#read", () => {
    it("should exist", () => {
      expect(read).to.be.a("function");
      expect(read).to.be.instanceOf(Function);
    });

    it("should return all tasks", () => {
      const mockTodos: ITask[] = [
        { id: "03f0zmxhnn0x", isDone: false, text: "first todo" },
        { id: "4csvd9s5w7u", isDone: true, text: "second todo" },
        { id: "2fcge5m7du6", isDone: false, text: "third todo" }
      ];

      const allTodos = read(mockTodos, "all");
      expect(allTodos).to.have.deep.members([
        { id: "03f0zmxhnn0x", isDone: false, text: "first todo" },
        { id: "4csvd9s5w7u", isDone: true, text: "second todo" },
        { id: "2fcge5m7du6", isDone: false, text: "third todo" }
      ]);
    });

    it("should return only completed tasks", () => {
      const mockTodos: ITask[] = [
        { id: "03f0zmxhnn0x", isDone: false, text: "first todo" },
        { id: "4csvd9s5w7u", isDone: true, text: "second todo" },
        { id: "2fcge5m7du6", isDone: false, text: "third todo" }
      ];

      const completeTodos = read(mockTodos, "complete");
      expect(completeTodos).to.have.deep.members([
        { id: "4csvd9s5w7u", isDone: true, text: "second todo" }
      ]);
    });

    it("should return only incompleted tasks", () => {
      const mockTodos: ITask[] = [
        { id: "03f0zmxhnn0x", isDone: false, text: "first todo" },
        { id: "4csvd9s5w7u", isDone: true, text: "second todo" },
        { id: "2fcge5m7du6", isDone: false, text: "third todo" }
      ];

      const incompleteTodos = read(mockTodos, "incomplete");
      expect(incompleteTodos).to.have.deep.members([
        { id: "03f0zmxhnn0x", isDone: false, text: "first todo" },
        { id: "2fcge5m7du6", isDone: false, text: "third todo" }
      ]);
    });
  });

  context("#update", () => {
    it("should exist", () => {
      expect(update).to.be.a("function");
      expect(update).to.be.instanceOf(Function);
    });

    it("should update 'isDone' to the opposite boolean", () => {
      const mockTodos: ITask[] = [
        { id: "03f0zmxhnn0x", isDone: false, text: "first todo" },
        { id: "4csvd9s5w7u", isDone: true, text: "second todo" },
        { id: "2fcge5m7du6", isDone: false, text: "third todo" }
      ];

      const newTodos = update(mockTodos, "4csvd9s5w7u");
      expect(newTodos).to.have.deep.members([
        { id: "03f0zmxhnn0x", isDone: false, text: "first todo" },
        { id: "4csvd9s5w7u", isDone: false, text: "second todo" },
        { id: "2fcge5m7du6", isDone: false, text: "third todo" }
      ]);
    });
  });

  context("#del", () => {
    it("should exist", () => {
      expect(del).to.be.a("function");
      expect(del).to.be.instanceOf(Function);
    });

    it("should delete the task with the requested id", () => {
      const mockTodos: ITask[] = [
        { id: "03f0zmxhnn0x", isDone: false, text: "first todo" },
        { id: "4csvd9s5w7u", isDone: true, text: "second todo" },
        { id: "2fcge5m7du6", isDone: false, text: "third todo" }
      ];

      const newTodos = del(mockTodos, "4csvd9s5w7u");
      expect(newTodos).to.have.deep.members([
        { id: "03f0zmxhnn0x", isDone: false, text: "first todo" },
        { id: "2fcge5m7du6", isDone: false, text: "third todo" }
      ]);
    });
  });
});
