import { ITask } from "./interfaces/todo.interfaces.js";
import { create, read, update, del } from "./controller/todo.controller.js";
import { printTasks, printHelp, printError } from "./view/todo.view.js";
import { saveToDB, checkFile } from "./dataBase/DB.js";
const pathDB = "./todos.json";
const myArgs = process.argv.slice(2);

(async () => {
  let tasks: ITask[] = await checkFile(pathDB);

  switch (myArgs[0]) {
    case "create":
      tasks = create(tasks, myArgs[1]);
      saveToDB(pathDB, tasks);
      break;

    case "read":
      tasks = read(tasks, myArgs[1] as "all" | "complete" | "incomplete");
      printTasks(tasks);
      saveToDB(pathDB, tasks);
      break;

    case "update":
      tasks = update(tasks, myArgs[1]);
      saveToDB(pathDB, tasks);
      break;

    case "delete":
      tasks = del(tasks, myArgs[1]);
      saveToDB(pathDB, tasks);
      break;

    case "help":
      printHelp();
      break;

    default:
      printError();
  }
})().catch(console.log);
